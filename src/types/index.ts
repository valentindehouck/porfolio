export type ProjectType = {
  url: string
  projectName: string
  dateCompleted: string
  description: string
  techStack: string[]
}
